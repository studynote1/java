# annotation

1. 注解的本质就是标签  注入解释
2. 注解使用的场景
        比如超市的商品
                牙膏的标签需要贴在牙膏的包装箱上 也可以贴在具体的牙膏上
                酒的标签需要贴在酒的包装箱上  也可以贴在酒瓶上
        FIELD
        constructor
        local_variable
        method
        package
        parameter
        annotation
        type
3. 注解有生效时期 retation
        比如超市的商品
                牙膏的标签 就是 在进行摆放货物的时候使用
                玻璃瓶酒的 易碎  标签 就是在 运输 搬运 的时候使用
        source 编译器处理时就丢掉了 class文件中不包含
        class  编译器处理时 添加到class文件中 但是不会被加载到jvm虚拟机中
        runtime class文件 jvm虚拟机中都有
4. 注解可以有值也可以无值
        如果同一类注解需要进一步区分 就需要赋值
        如果不用区分  就不需要赋值
        比如超市的商品
                牙膏是一个标签  黑人牙膏就是标签的一个值
                @interface yagao{ String name();}
                那么在给牙膏类加注解的时候  就需要赋一个值 @yagao("黑人") @yagao("高露洁")
5. 注解使用
        编译期使用
                会把注解添加到class文件的域中 或是方法上
        运行期使用 反射
                field 遍历对象的域属性 判断域是否属于对应注解类型 如果是,就可以通过field.getAnnotation(Annotation.class)获得这个注解类
                不过注解的大部分时候使用都仅仅是为了判断，而不是取值。（标签的本质）。当然也可以取值,用来拓展功能

6. 注解处理

        所有的程序元素 包括class method constructor等类都实现了java.lang.reflect.AnnotatedElement这个接口,就相当是生命这些元素出场时都要支持注解。相当于是一个规范所有出厂的产品必须要贴标签。

7. spring对注解的使用

        spring框架中有专门处理注解的处理器 annotationbeanpostprocessor
