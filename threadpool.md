# 线程池的使用

## 模型

将任务和线程分开

比如：  
iphone实体店   
通常实体店有coreThreadNum个工作人员服务于某个实体店  1个工作人员服务一个客户  
客户来了,没有工作人员处于空闲，需要进行排队  workqueue  
如果客户比较多，队列超过了最大处理长度，coreThreadNum个工作人员处理不了，这时总部就需要向实体店添加新的工作人员。maxThreadNum  
如果客户多到maxThreadNum个工作人员也处理不完，这个时候就要告诉新来的客户，让他们回去，可以明天再来，也可以去其他地方。 reject 

## 参数

1. 核心线程数

线程池中常驻的线程数量

2. 最大线程数

满负载时线程的数量  

3. 超时时间

线程的最大空闲时间，空闲多久后进行关闭  
仅对超过核心线程数量的线程起作用，不大于核心线程数量的线程不做处理，不会停止  

如果想在核心线程空闲一段时间后停止，可以使用这个方法来设置，

allowCoreThreadTimeOut(timeout)

如果timeout为0  表示不生效，即不会终止核心线程
如果timeout大于0  表示立即生效  在空闲这个时间之后，会终止核心线程，调用这个方法  

interruptIdleWorkers()

4. 阻塞队列

向线程池中添加任务时，会放到阻塞队列中，然后线程池从阻塞队列中获取可以执行的任务

5. 线程组

用于标识线程池中的一组线程

6. 拒绝处理

添加任务时，当最大线程数已经跑满，并且阻塞队列也已经满了的情况下，线程池就会拒绝执行任务。

## 用法

1. 拓展

- beforeExecute()
- afterExecute()

2. 添加任务

execute(Runnable)

3. 移除任务

remove(Runnable)
purge()

4. 结束线程池

shutdown()
shutdownNow()

5. 查看活着的线程

getActiveCount()

遍历所有的worker，判断是否上锁，如果上锁，说明线程正在执行，如果没有上锁 ，说明线程正在休眠

6. 查看完成的任务的数量

getCompletedTaskCount()

7. 获取阻塞队列中的任务

getQueue()

8. 获取任务数量

getTaskCount()

这个任务数量包括，已经每个worker完成的任务数量+正在执行的任务数量+任务队列中的数量

9. 查看线程池是否结束

- isShutdown()  
- isTerminated()  
- isTerminating()  
- awaitTermminating(timeout)

# 线程池的源码解析

[好文推荐，本文图解来自如下链接](https://www.cnblogs.com/trust-freedom/p/6681948.html#label_3_1)

0. 线程池的主要变量

- blockingqueue<Runnable> workqueue

    任务队列

- hashset<Workers> workers

    处理任务的机器 Runnable放到workers可以被执行

- 

1. 线程池状态 

第一个任务添加后，线程池的状态会改变为Running  
如果没有执行过shutdown或者shutdownNow 线程池会一直保持着running状态，哪怕当前的线程数为0

![状态转换流程](https://images2015.cnblogs.com/blog/677054/201704/677054-20170408210803050-1576156526.png)

private final AtomicInteger ctl = new AtomicInteger(ctlOf(RUNNING, 0));

RUNNING

    111+29个0  高3位位1  低29位为0
    初始状态，可以新增任务，可以执行任务

SHUTDOWN 0

    32位全0 
    不接受新任务，但可以处理已经有的任务
    RUNNING+shutdown() => SHUTDOWN

STOP

    001+29个0
    不接受新任务，中断所有已经有的任务
    RUNNING + shutdownNow() => STOP
    SHUTDOWN + shutdownNow() => STOP

TIDYING

    010+29个0
    所有的任务都被停止
    SHUTDOWN+队列任务为空+执行任务为空 => TIDYING
    STOP+执行队列为空 => TIDYING

TERMINATED 

    100+29个0
    线程池已经终止
    TIDYING + terminated() => TERMINATED

CAPACITY

    000+全1
    线程池最大的线程数 2^29 个
    低29位存储线程的数量

2. Worker

线程池为了对Runnable添加控制，实现中断，添加了一个Worker的内部类，实现了Runnable, 拓展了Thread，在worker类中可以运行任务

Worker 中有Runnable  Thread 两个主要变量    
还有一个completedTasks一个用来统计的变量  

Worker 类实现了Runnable 说明其是一个任务
继承了AQS 说明了一个任务就是一把锁，不可重入，保证一个任务只能被一个线程执行和中断。  
AQS 在Workwer初始化时被置为 -1

Worker其实是对Runnable的一个代理

主要的方法有

- lock

    任务执行前都要加锁
    将state置1

- unlock

    任务完成后释放锁
    将state置0

- tryLock

    尝试获取锁
    CAS 把state 由0置1

- isLocked

    判断是否上锁 如果上锁说明线程正在执行
    state=0  表示被锁定
    state>0  表示没被锁定

- interruptIfStarted

    终止运行的线程  只有当state大于0时，线程才能被中断
    调用shutdownNow时会触发这个方法 
    shutdown方法只有在state为0的时候才能中断线程  因为她首先要调用tryAcquire  通过CAS获取锁state   

```java
private final class Worker
        extends AbstractQueuedSynchronizer
        implements Runnable
    {
        /**
         * This class will never be serialized, but we provide a
         * serialVersionUID to suppress a javac warning.
         */
        private static final long serialVersionUID = 6138294804551838833L;

        /** Thread this worker is running in.  Null if factory fails. */
        final Thread thread;
        /** Initial task to run.  Possibly null. */
        Runnable firstTask;
        /** Per-thread task counter */
        volatile long completedTasks;

        /**
         * Creates with given first task and thread from ThreadFactory.
         * @param firstTask the first task (null if none)
         */
        Worker(Runnable firstTask) {
            setState(-1); // inhibit interrupts until runWorker
            this.firstTask = firstTask;
            this.thread = getThreadFactory().newThread(this);
        }

        /** Delegates main run loop to outer runWorker  */
        public void run() {
            runWorker(this);
        }

        // Lock methods
        //
        // The value 0 represents the unlocked state.
        // The value 1 represents the locked state.

        protected boolean isHeldExclusively() {
            return getState() != 0;
        }

        protected boolean tryAcquire(int unused) {
            if (compareAndSetState(0, 1)) {
                setExclusiveOwnerThread(Thread.currentThread());
                return true;
            }
            return false;
        }

        protected boolean tryRelease(int unused) {
            setExclusiveOwnerThread(null);
            setState(0);
            return true;
        }

        public void lock()        { acquire(1); }
        public boolean tryLock()  { return tryAcquire(1); }
        public void unlock()      { release(1); }
        public boolean isLocked() { return isHeldExclusively(); }

        void interruptIfStarted() {
            Thread t;
            if (getState() >= 0 && (t = thread) != null && !t.isInterrupted()) {
                try {
                    t.interrupt();
                } catch (SecurityException ignore) {
                }
            }
        }
    }
```

3. addWorker()

![添加任务流程](https://images2015.cnblogs.com/blog/677054/201704/677054-20170408211358816-1277836615.png)

```java
private boolean addWorker(Runnable firstTask, boolean core) {
        retry:
        for (;;) {
            int c = ctl.get();
            int rs = runStateOf(c);

            // Check if queue empty only if necessary.
            if (rs >= SHUTDOWN &&
                ! (rs == SHUTDOWN &&
                   firstTask == null &&
                   ! workQueue.isEmpty()))
                return false;

            for (;;) {
                int wc = workerCountOf(c);
                if (wc >= CAPACITY ||
                    wc >= (core ? corePoolSize : maximumPoolSize))
                    return false;
                if (compareAndIncrementWorkerCount(c))
                    break retry;
                c = ctl.get();  // Re-read ctl
                if (runStateOf(c) != rs)
                    continue retry;
                // else CAS failed due to workerCount change; retry inner loop
            }
        }

        boolean workerStarted = false;
        boolean workerAdded = false;
        Worker w = null;
        try {
            w = new Worker(firstTask);
            final Thread t = w.thread;
            if (t != null) {
                final ReentrantLock mainLock = this.mainLock;
                mainLock.lock();
                try {
                    // Recheck while holding lock.
                    // Back out on ThreadFactory failure or if
                    // shut down before lock acquired.
                    int rs = runStateOf(ctl.get());

                    if (rs < SHUTDOWN ||
                        (rs == SHUTDOWN && firstTask == null)) {
                        if (t.isAlive()) // precheck that t is startable
                            throw new IllegalThreadStateException();
                        workers.add(w);
                        int s = workers.size();
                        if (s > largestPoolSize)
                            largestPoolSize = s;
                        workerAdded = true;
                    }
                } finally {
                    mainLock.unlock();
                }
                if (workerAdded) {
                    t.start();
                    workerStarted = true;
                }
            }
        } finally {
            if (! workerStarted)
                addWorkerFailed(w);
        }
        return workerStarted;
    }
```

4. execute()

![execute流程](https://images2015.cnblogs.com/blog/677054/201704/677054-20170408210905472-1864459025.png)

1. 任务来了，先判断线程是否达到了corePoolSize，如果没有，addWorker启动新线程处理任务

    如果启动新线程返回成功，直接返回  
    如果失败，继续往下执行

2. 如果达到了corePoolSize，就要把任务放到阻塞队列里面,放到阻塞队列之前要先判断线程池是否已经处于终止状态，因为终止状态不允许接受新的任务到任务队列

    如果阻塞队列中添加成功，重新检查线程池是否还在运行，
        
        如果线程池已经结束，就把新增的任务再从队列中移除，并且拒绝这个任务
        如果线程池未结束，并且工作的线程数少于corePoolSize,直接启动新的线程处理这个任务

3. 如果线程池已经关闭，或是阻塞队列已满，则调用addWorker直接启动新的线程处理任务
    
    如果启动新线程也返回失败，那就拒绝这个任务

```java
public void execute(Runnable command) {
    //任务为空抛出异常
    if (command == null)
        throw new NullPointerException();
    //获取线程池的状态
    int c = ctl.get();
    //判断运行的线程是否够常驻线程数
    //如果不够，直接添加worker
    if (workerCountOf(c) < corePoolSize) {
        if (addWorker(command, true))
            return;
        c = ctl.get();
    }
    //如果不够常驻线程数 判断线程池是否在执行
    //如果线程池在执行  向阻塞队列中添加任务
    if (isRunning(c) && workQueue.offer(command)) {
            int recheck = ctl.get();
            if (! isRunning(recheck) && remove(command))
                reject(command);
            else if (workerCountOf(recheck) == 0)
                addWorker(null, false);
    }
    //如果线程池没有在运行，那就直接添加worker
    else if (!addWorker(command, false))
        reject(command);
}
```

5. reject()

6. runWorker()

![执行任务流程](https://images2015.cnblogs.com/blog/677054/201704/677054-20170408211458878-1033038857.png)
