# biginteger

## 组成

signum  表示正负
mag[] int数组组成 表示这个数

# static filed

1. LONG_MASK

掩码  确保int或是long 不区分正负  相加减用位移操作
将int long按无符号数处理

## public method

1. intValue

        超出范围会截断
        取mag数组的第一个int值 mag[0]

2. intValueExact

        超出范围 报错
3. add

加法 先判断正负0
如果同正负 直接对int数组做位移加法
如果不同正负  对int数组做位移减法

## private method

1. getInt(n)

取mag数组下标为length-1-n的int值  从后往前  越往后越小
如果n小于0  返回0 异常情况
如果n大于mag数组的长度  返回-1 或是 0  异常情况
? 如果n 小于 第一个不是0的int的下标 取这个int值的负值  如果大于  取这个int的非值

2. add(int[] x, int[] y) -> int[]

位移加法
1. long可以保存int加法的进位和结果 低32位表示加法结果 33位表示进位
2. 从后向前做加法  加上进位结  sum = a + b + sum>>>32