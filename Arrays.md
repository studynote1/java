# System

System.arrayCopy(T[] src, int srcPos,T[] dst, int dstPos,int len);

向前复制  向后复制 调用 copyArray(char* src,char* dst,int len) c++函数  指针操作
如果src>dst 会出现向后复制  否则向前复制
浅拷贝 对象拷贝指针 基本数据类型拷贝值

# StringBuilder

delete replace 都用到了arrayCopy方法

# Arrays

## deepToString  深度遍历多重数组

deepToString(Object[] src,StringBuilder sb,Set<Object[]> deJaVu);
dejavu 似曾相识 之前已经遇到过  防止循环调用
递归输出多重数组
StringBuilder 非线程不安全

## toString  如果有数组嵌套 输出地址

## binarySearch

//assume src is ordered
//assume range check
binarySearch(int[] src,int from,int to,int key){
        int low=from;
        int high=high;
        while(low<high){
                mid = (low+high)>>1;
                midVal = src[mid];
                if(midVal>key){
                        high=mid-1;
                }else if(midVal<key){
                        low=mid+1;
                }else{
                        //find
                }
        }
}

## copyOf

赋值数组并返回  调用System.arrayCopy()

## fill

用某个值给数组赋值

fill(int[] src,int val)

## parallelPrefix

对数组的每个元素的上一个元素都执行对应的操作
int[] a = {1,2,3,4,5,6,7,8}
parallelPrefix(a,(i,j)->{return i+j;})
j是当前遍历元素  i是元素的上一个元素
从下标1开始
2->1+2
3->3+3(1+2 +3)
4->6+4(1+2+3 +4)

## sort

## stream

IntStream 等这些是Stream针对基本数据类型的优化版本 优先使用

## spliterator

分隔迭代
将一个迭代器分割成多个迭代器
1. 可以由多线程并发分别处理其中的某段
2. 拆分出其中的某段

# 排序

DualPivotQuickSort

多种排序算法
长度大于286  使用归并
小于286  大于47  使用快排
小于29 使用插入排序

byte 和 short 使用桶排序



# Arrays Collections

Arrays 是针对数据的工具类
Collections 是针对集合的工具类