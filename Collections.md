# Collection 框架

## 总描述

顶级 Iterable 接口 功能性的接口 遍历使用 任何集合都要是可迭代的

Collection 基本有3个类型的接口

List
Queue
Set

## Iterable

- iterable
        迭代功能
- foreach
        边迭代边操作
- spliterator
        分隔迭代 拆分成小集合

## Collection

- 继承了 Iterable 接口
        表明集合都是可迭代的
- 新增
- 移除
- 清空
- 大小
- 是否包含
- 是否为空
- 装换为数组
- 装换为strea流
- 取交集

## List

继承自 Collection

首先继承了可迭代
继承了Collection的基本功能
添加了索引功能 既有索引又可以迭代

## Set

完全继承了Collection
添加了先进先出功能

- add/offer
        插入到队尾
        add抛异常 offer内部处理异常
- remove/poll
        弹出队首 并返回
        remove抛异常  poll异常处理
- element/peek
        返回队首 不移除
        element抛异常 peek异常处理

### Deque

双向队列
继承Queue

## Vector

通过list实现

## Map

map是对set的一种封装 通过entrySet实现Collection接口