# 端口扫描

## nc

### 参数

- v

输出详细信息

- z

## 原理

### 端口的状态

open  端口开放  可以访问
closed  没有程序监听  有返回包  比如防火墙设置 reject
filtered  不返回  这个包被丢弃 比如防火墙设置 drop

### 扫描方式

- tcp扫描

        如果能建立连接，说明open
        如果不能建立连接，说明closed filterd

- 半连接扫描

        发送syn 确认ack后 就发送rst 断掉连接

- FIN扫描

        发送FIN标记位
                端口open 会丢弃
                端口closed 会响应RST
                端口FILTERED 会丢弃

- Xmas扫描

        标记位都置为1
                端口open 丢弃
                端口closed 响应RST
                端口filtered  丢弃

- NULL扫描

        标志位都置为0
                unix 端口 open or filtered  丢弃
                unix 端口 closed  返回RST
                windows 不管open 还是closed filtered 都返回 RST

- FTP弹跳扫描
- 无状态扫描

# 服务器安全设置

1. 防火墙 限制登录IP和端口
2. 文件访问控制
3. 数据库的访问控制


# 目前可能的风险

1. 获取到shell之后
2. 查看jar文件 获取到数据库密码  导库的风险