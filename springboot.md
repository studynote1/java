# 启动流程

- 启动图

![picture](https://upload-images.jianshu.io/upload_images/6912735-51aa162747fcdc3d.png?imageMogr2/auto-orient/strip)

- 分3部分

1. SpringApplication 的初始化

new SpringApplication(sources)->initialize(sources)->
包括基本的环境变量  资源 构造器 监听器

ApplicationListener  事件监听器  (run  refreshContext  注册)
ApplicationContextInitializer  应用上下文配置(run prepareContext applyInitiallizers中被调用)
mainApplicationClass
WEB_ENVIRONMENT_CLASSES

2. 应用的创建和启动

SpringApplication.run()

启动流程的监听模块 加载配置环境

SpringApplicationRunListeners
ConfigurableEnvironment
ConfigurableApplicationContext

    1. 创建上下文
    2. 预处理上下文

        1. 设置环境变量
        2. 利用上面的initializer对context初始化
        3. 上面的监听器激活context 就绪事件
        4. 注册xml或者javaConfig中的bean
        4. context 加载
        5. listeners 激活 contextloaded 事件

    3. 刷新上下文

        1. 注册beanFactory
        2. 创建servlet容器  onRefresh createEmbeddedServletContainer
        2. 注册监听器
        3. beanFactory加载实例化所有的单例类
        4. 设置bean的生命周期处理器  用于对实现了lifecycle接口的bean的生命周期进行管理

    4. 再次刷新上下文

        1. applicationContext子类的拓展
        2. listeners 激活 contextfinished 事件
        3. 调用callRunner函数

    5. 发布上下文就绪事件到监听器

3. 自动化配置模块

- bean的加载

finishBeanFactoryInitialization()->
preInstantiateSingletons()->
getSinfleton()->  3级缓存  singletonObjects-> earlySingletonObjects-> singletonFactorys
getBean()->
initializeBean()

# spring

1. applicationContext 和 beanFactory 都是spring的容器
applicationContext是beanFactory的子接口
把beanFactory比作心脏的话，applicationContext是spring的整个躯体

## springboot tomcat-embed

1. onRefresh() -> createEmbeddedServletContainer()
2. 获取 EmbeddedServletContainerFactor
3. EmbeddedServletContainerFactory 获取相应的容器
        getEmbeddedServletContainer()
4. 创建tomcat对象
        创建baseDir
        创建connector
        tomcat 添加connector
        自定义connector
        设置engine
        添加额外的connector
        准备上下文  servletInitializer

                设置name
                设置path
                设置docbase
                设置生命周期监听器
                设置资源加载器
                设置区域化mapping
                设置webapploader
                添加默认的servlet  添加servletMapping
                配置 context 上下文
                        serverProperties 读取 yml文件的 server 配置

5. 返回容器 getTomcatEmbeddedServletContainer(tomcat)
        初始化容器
        tomcat.start()
6. initPropertySources
        设置servletContext的值

## url匹配

1. tomcat ApplicationDispatcher  dispatch()方法处理

## context的关系

tomcat 下 有servletContext 容器  创建tomcat等容器时创建
servletContext 下 有 webApplicationContext 容器 ContextLoaderListener创建
webApplicationContext 下有 dispatcherServlet(mvc) 等servlet

所以请求由tomcat发给servletContext servletContext发给WebApplicationContext下面的dispatcherServlet 实 现转发 到具体处理的servlet

所有用户公用一个servletContext   1个用户1个session  1个用户多个request

1个controller 就是一个servlet

![关系](http://www.it165.net/uploadfile/files/2015/0201/2015020117491892.png)

## springboot 配置文件加载

1. run -> prepareEnvironment
        1. 获取environment
                解析加载默认的配置文件
                        servletconfiginitparams
                        servletcontextinitparams
                解析加载系统文件配置和环境变量
                        systemproperties
                        systemenvironment
        2. 配置environment configureEnvironment()
                1. configurePropertysources
                        1. 配置默认的配置文件 PropertySource
                        2. 解析加载命令行指定的参数和配置 SimpleCommandLinePropertySource
                                优先级最高 添加PropertySource之后会触发排序
                2. configureprofiles
        3. 监听器响应环境准备好的事件 listeners.environmentPrepared()
                1. multicastEvent  ApplicationEnvironmentPreparedEvent
                2. invokeListener(listener,event)
                3. doinvokelistener(listener,event)
                4. listener.onApplicationEvent(event)
                5. onApplicationEnvironmentPreparedEvent
                        1. loadPostProcessors
                                默认的json
                                cloudfoundaryvcap
                                ConfigfileApplicationListener  加载 application.yml 用的这个
                        2. postProcessEnvironment
                                addPropertySources
                                        randomValuePropertySources
                                        new Loader().load
                                                getSearchLocations()
                                                        file:./config/ > file:./ > classpath:./config/ > classpath:./
                                                getSearchNames()
                                                        application
                                                propertiesLoader.getAllFileExtensions()
                                                        properties > xml > yml > yaml
                2. postProcessEnvironment
                        1. addPropertySources
                        2. configureIgnoreBeanInfo
                        3. bingToSpringApplication

2. Environment 对象

        1. 属性
                1. PropertySource 属性源
                2. PropertyResolver 属性解析器 解析响应的key和value
                3. Environment 继承自PropertyResolver 提供了Profile特性
                4. Profile 剖面 只有激活的配置才会被注册到spring容器中

3. --spring.config.location=path/application.properties

4. [链接](https://blog.csdn.net/z_ssyy/article/details/105347680)

## aware接口

回调传参的一个接口
有一个set方法 把相应的对象传到对象中去
会在对象初始化，给对象传入一个值

## bean的作用域

1. singleton 单例 整个容器只有1个
2. prototype 单个调用 每次从容器中调用getBean时 都返回1个新的实例
3. request(web) 每个http请求
4. session(web) 同一个httpSession
5. globalsession(web) 一般用于portlet

## bean的生命周期

[链接](https://blog.csdn.net/qq_35956041/article/details/81588160)

# spring的核心

1. beans
        beanFactory
2. context
3. core
        environment
        conversionService 类型转换服务
                反射
                泛型
                Class
        Resource
                基于协议加路径的输入流
                file:
                classpath:
                jar:
        序列化 反序列化
        访问类型元数据

# tomcat

tomcat 对外 提供可以访问的协议接口
tomcat 对内 提供可以编程的servlet接口
所以tomcat其实就是一个耦合器
request => tomcat
           tomcat  => servlet
           tomcat  <= servlet
response <= tomcat

spring 其实就是一堆的servlet的实现的组合

# beanPostProcessor

这个接口是spring 提供给bean进行拓展的一个接口
有2个方法
        postProcessBeforeInitialization
        postProcessAfterInitialization
可以在bean的初始化之前和之后分别做些自定义操作

## bean 的生命周期

1. 实例化bean
2. 设置属性
3. 设置beanName 实现了BeanNameAware接口
4. 设置beanFactory 实现了BeanFactoryAware接口
5. 设置applicationContext 实现了ApplicationContextAware接口
6. 执行postProcessBeforeInitialization  实现了BeanPostProcessor接口
7. 执行InitializingBean.afterPropertiesSet
8. 执行 init-method
9. 执行postProcessAfterInitialization  实现了BeanPostProcessor接口

