# rocketmq

1. 消息过滤 t
        broker过滤 tag过滤 header过滤 body过滤
        consume过滤  自定义
2. 消息持久化 保证宕机后  可以恢复
3. 可靠性
        对应broker正常关闭 异常关闭 系统异常关闭都是可以通过重启恢复的
        对于机器无法开机  磁盘损坏的情况下  如果没有主从 主备  数据就全部丢失 且不可恢复
4. 回溯消费
        对于已经消费过的消息  可以让consume重新消费 几个小时 前的数据
5. 消息堆积
        消息能堆积多少条 堆积后的生产者的吞吐量  堆积后的消费者的吞吐量
6. 事务
        rocketmq通过offset来实现事务回滚  会使系统的脏页增多
7. 定时消息
        rocketmq支持 特定的时间精度
8. 消息重试


## 架构

1. nameserver
        分布式  消息负载均衡
        topic路由到队列的broker
2. broker 队列核心
        broker 启动后需要注册到 nameServer
        每隔30s上报topic路由信息
3. producer 消息生产者
        上传消息时需要从nameserver获取topic路由消息
        根据路由信息选择队列进行消息发送
        producer 和 nameserver 保持长连接
4. consumer 消息消费者
        push 注册到nameserver 等待broker唤醒
        pull 不断向broker请求
        consumer 会和 包含topic队列的 broker 保持长连接
        consumer 和 nameserver 保持长连接


## 配置

- broker

1. deleteWhen=04
        删除文件的时间点 默认凌晨4点
2. fileReservedTime=120
        文件保留时间
4. commitLog=
        每个文件的默认大小
5. storePathCommitLog=
        commitlog存储路径
6. storePathConsumeQueue=/usr/local/rocketmq/store/consumequeue
        消息队列的存储路径

## 理解

rocketmq 就是把producer生产的消息存储到commitLog文件中
队列文件是对commitLog文件的索引和偏移
通过同步和异步刷盘将磁盘的文件加载到内存中用于消费者使用