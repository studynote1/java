# aop

## 概念

1. jointpoint

代理类
核心
aop通过这个类实现对被代理方法的执行
主要是proceed

仅作为理解使用
```java
class jointpoint{
        //被代理对象
        pointcut p;

        jointpoint(pointcut p){
                this.p=p;
        }

        //执行代理的方法
        invoke(){
                p.hhh();
        };
}
```

2. pointcut

指定被代理类
用于指定jointpoint可以代理哪些方法

仅作为理解使用
```java
interface pointcut{
        hhh();
}
```

3. advice

代理时机
before 在被代理方法前执行
after 被代理方法后执行
around 之前和之后都执行
after returning 被代理方法正常完成后执行
after throwing 被代理方法异常后执行
after finally 被代理方法正常或异常后执行
introduction 为原有的对象添加新的属性或行为

```java
class advice{
        before(){};
        after(){};

        jointpoint j;
        advice(jointpoint j){
                this.j=j;
        }

        invoke(){
                before();
                j.invoke();
                after();
        }
}
```

4. aspect

是否启用aop 已经启用aop后的各个组件的封装
把jointpoint pointcut advice等封装成一个切面注入到工程中(spring)


# 代理

实现代理必须有的3个元素(jointpoint接口中必须要)
target 被代理对象
method 被代理方法
args 被代理方法的参数

```
method.invoke(target,args)
```

## 种类

静态代理  编译之前就已经确定了 生成实际的class文件
动态代理  编译后被代理 没有class文件 只有类字节码 直接加载到虚拟机
  jdk动态代理
        通过java反射机制实现
        ```
                method.invoke(target,args)
        ```
        ```java
                interface InvocationHandler{
                        //当代理出来的对象执行代理方法时，关联到的invocationHandler对象
                        //proxy不知道？ 感觉应该是代理出来的对象
                        public Object invoke(proxy,method,args){
                                Object retObj = method.invoke(target,args);
                                return retObj;
                        }
                }
        ```

  cglib动态代理
        通过继承被代理对象，重写方法，生成类字节码实现代理 比反射效率高
        intercept() 方法来代替原原方法的执行
        ```java
                //obj不知道？ 感觉应该是生成的代理对象
                //proxy不知道？ 代理的方法 MethodProxy
                interface MethodInterceptor{
                        public Object intercept(obj,method,args,proxy){
                                Object retObj = method.invoke(target,args);
                                return retObj;
                        }
                }
        ```

### cglib类比

class cglib{
        //所有的cglib代理对象都有的属性
        sig1;
        sig2;

        //被代理的对象才有的属性
        sig3;
        sig4;
}
这样代理公司可以通过sig1,sig2等属性很方便的知道这个对象是不是自己公司代理出来的。
cglib不是通过反射来实现方法的调用，而是通过在sig中维护一个方法数组，通过索引直接进行方法的调用

### cglib和jdk的区别
        jdk代理的类必须实现1个或多个接口
        cglib代理类不需要实现接口，但是不能是final类(继承和重写方法)

### 静态代理和动态代理的区别

        静态代理相对简单 但是拓展性不强 效率比动态代理好
        动态代理使用相对灵活

### cglib的原理

        1. 通过Enhancer类生成代理类的字节码
        2. 加载字节码，生成class对象
        3. 反射获得实例 创建代理对象

## 代理类一般要实现2个方法，1个属性
        1. 属性 被代理类 Object object
        2. 生成代理对象的方法 newProxyInstance getProxyInstance
        3. 代理对象调用方法时的回调方法 invokeHandler中的invoke 和 MethodInterceptor中的intercept

## 代理设计模式设计用到的元素
代理类 classloader其实就是1个代理
被代理类 ClassA
生成的代理的对象 ClassA a;
nvocationHandler会调接口 a.invoke();