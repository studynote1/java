# nginx

## nginx配置

### 配置拆分

1. conf/ 文件夹下新建 conf.d 文件夹
2. 新建配置文件 xxx_location.conf
3. 将原来 nginx.conf 中的 location 移到 xxx_location.conf 中
4. nginx.conf 注释掉原来的配置 添加  include xxx_location.conf; (注意路径)
5. 检查配置 nginx -t
6. 重新生效配置  nginx -s reload

### 日志输出

- 配置

1. log_format  log_name  '$remote_addr $remote_user';
2. access_log  file_name log_name;

- 说明 (!https://www.cnblogs.com/wajika/p/6426270.html)

remote_addr  客户端IP地址
remote_port 客户端端口
uri  请求中的uri 不包含主机名
args 请求参数
host 请求的服务主机名
request_body 请求主体
request_method  请求方法

remote_user 客户端用户
time_local  本地时间
request  请求的url和协议
status  请求的状态
body_bytes_send  发送给客户端的字节数
connection_requests  通过一个连接获得的请求数量
msec  日志写入时间
http_referer  哪个页面链接过来的
http_user_agent  客户端浏览器的相关信息
request_length 请求长度
request_time  请求处理时间
http_x_forword_for
upstream_addr
upstream_response_time
request_time

### nginx获取自定义头部

1. 开启

        underscores_in_headers on;

2. $http_header_name 引用即可

### 服务治理

1. 逐步使每个服务在header中添加 caller_service callee_service标识调用方和被调用方
2. nginx中获取 service_name 进行记录调用者和被调用者 caller_service call callee_service:url  可以考虑放入数据
3. 如果需要添加服务调用权限管理 可以加一个认证机制 caller_service_key 在脚本中验证

- 作用

记录服务间的调用关系  方便在被调用服务升级 有改动时 可以通知到 调用方
可以使用sqllite作为存储数据库