# dockeru安装使用

## Ubuntu wsl 安装

### 移除旧版本

sudo apt-get remove docker docker-engine docker-ce docker.io

### 添加官网密钥

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

### 设置存储库

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"


### 更新包索引

apt uddate

### 查看可用版本

apt-cache madison docker-ce

### 安装

版本 18.06.0 没有问题
sudo apt-get install docker-ce=18.06.0~ce-0~ubuntu

### 启动服务

要运行docker需要以管理员权限启动wsl

service docker start
service docker status
service docker stop

### 运行镜像

docker run -p host-ip:host-port:container-port docker-name

#### 参数

- d

后台运行

- i

交互方式启动

- t

- p

端口映射

-v host_dir:container_dir

挂载目录 将宿主机的目录挂载到容器中 这样容器可以直接访问宿主机目录

## 命令

- 启动镜像

docker run docker-name or docker-id

- 停止容器

docker stop docker-name or  docker-id

- 查看镜像

docker images

- 查看容器

docker ps

        - a

                查看所有的容器 包括启动的和未启动的

- 查看镜像详情

docker inspect docker-name

# docker架构

[网址](!https://segmentfault.com/a/1190000009309297)

docker client
docker server daemon
docker engine

docker有dockerclientk进程  server守护进程  engine进程

docker 通过 rest api 向dockerd发送请求和命令
dockerd 接收到命令后 通过rest api的方式向 docker-hub 发送请求
dockerd 接收到命令后 通过grpc的方式 向docker-containerd 发送命令
docker-containerd 向 docker-containerd-shim 发送命令 docker-containerd 管理本地所有正在运行的容易 docker-containerd-shim只负责一个运行的容易 docker-containerd-shim相当是是对runc的一个封装
docker-containerd-shim 按照runtime的标准准备好相关的运行时环境  启动该runc进程
docker-runc 进程打开容器的配置文件 找到rootfs的位置 启动相应的进程

其中dockerd 和 docker-containerd 是后台常驻进程 docker-containerd-shim 和 容器启动的进程是containerd 按需启动

## 核心原理

namespace 控制 资源隔离
cgroups 控制 资源限制

## docker目录使用

1. 目录挂载 这个不方便迁移 因为多台主机的目录可能不一致 将宿主机的目录挂载到容器的目录下
2. 卷挂载 可以再dockerfile中配置可以方便迁移 卷由docker管理 所有的卷都在docker的某个目录下 不受宿主机的影响 将某个卷挂载到容器的目录下